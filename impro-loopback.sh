#!/bin/bash

# You need to install psmisc package to have killall command
# sudo apt install psmisc

# You need to install swctl client
# 
# sudo apt update 
# sudo apt install -y python3-pip
# pip install python-socketio websocket-client
# sudo wget -O /usr/local/bin/swctl https://gitlab.com/sat-mtl/tools/switcher/-/raw/develop/tools/swctl
# sudo chmod +x /usr/local/bin/swctl

# Install ssh keys on machine to allow for password-less ssh commands
# We install our own key locally to allow for local ssh commands, so script 
# can be run from either node.
#
# ssh-keygen
# ssh-copy-id scenicbox@scenicbox1.tail12345.ts.net
# ssh-copy-id scenicbox@scenicbox2.tail12345.ts.net

# Test ssh connection one time to accept remote ssh server fingerprint
#
# ssh scenicbox@scenicbox1.tail12345.ts.net
# ssh scenicbox@scenicbox2.tail12345.ts.net

NODE1_SSH_USER="scenicbox"
NODE1_HOSTNAME="scenicbox1.tail12345.ts.net"
NODE1_URI="http://${NODE1_HOSTNAME}:8000"
NODE1_SIP_USER="sipuser1"
NODE1_SIP_PASSWORD="sip-password1"

NODE2_SSH_USER="scenicbox"
NODE2_HOSTNAME="scenicbox2.tail12345.ts.net"
NODE2_URI="http://${NODE2_HOSTNAME}:8000"
NODE2_SIP_USER="sipuser2"
NODE2_SIP_PASSWORD="sip-password1"

SIP_SOURCE_PORT="5060"
SIP_SERVER_PORT="5060"
SIP_SERVER="sip.scenic.sat.qc.ca"

# Resolutions: 0 (1920x1080), 1 (1280x720)
VIDEO_RESOLUTION="0"
# Framerates: 0 (60 fps), 3 (30 fps)
VIDEO_FRAMERATE="3"
VIDEO_BITRATE="4096"

info() {
  echo -e "\e[32m\t${1}\e[0m"
}

# Close browsers
info "Closing Node 1 browser..."
ssh ${NODE1_SSH_USER}@${NODE1_HOSTNAME} killall chrome
info "Closing Node 2 browser..."
ssh ${NODE2_SSH_USER}@${NODE2_HOSTNAME} killall chrome

# Restart Switcher engines
info "Restarting Node 1 Scenic..."
ssh ${NODE1_SSH_USER}@${NODE1_HOSTNAME} docker restart scenic-core
info "Restarting Node 2 Scenic..."
ssh ${NODE2_SSH_USER}@${NODE2_HOSTNAME} docker restart scenic-core
info "Waiting 4 seconds before starting configuration..."
sleep 4

# Start Scenic GUIs
info "Starting Node 1 Scenic GUI..."
ssh ${NODE1_SSH_USER}@${NODE1_HOSTNAME} "DISPLAY=:0 scenic-open &>/dev/null" &
info "Starting Node 2 Scenic GUI..."
ssh ${NODE2_SSH_USER}@${NODE2_HOSTNAME} "DISPLAY=:0 scenic-open &>/dev/null" &

# Load Scenic bundles configuration
info "Loading Scenic bundles configuration..."
swctl --uri ${NODE1_URI} --load-bundles ~/.config/scenic/bundles.json
swctl --uri ${NODE2_URI} --load-bundles ~/.config/scenic/bundles.json

# Setting Node 1 SIP
info "Creating Node 1 SIP quiddity..."
swctl --uri ${NODE1_URI} --create-quiddity sip sip
info "Setting Node 1 SIP access list mode to everybody..."
swctl --uri ${NODE1_URI} --set-prop sip mode 1
info "Setting Node 1 local SIP source port..."
swctl --uri ${NODE1_URI} --set-prop sip port ${SIP_SOURCE_PORT}
info "Setting Node 1 STUN/TURN servers..."
swctl --uri ${NODE1_URI} invoke sip set_stun_turn ${SIP_SERVER} ${SIP_SERVER} ${NODE1_SIP_USER} ${NODE1_SIP_PASSWORD}
info "Registering Node 1 SIP..."
swctl --uri ${NODE1_URI} invoke sip register ${NODE1_SIP_USER}@${SIP_SERVER}:${SIP_SERVER_PORT} ${NODE1_SIP_PASSWORD}
info "Adding Node 1 remote SIP contact..."
swctl --uri ${NODE1_URI} invoke sip add_buddy ${NODE2_SIP_USER}@${SIP_SERVER}

# Setting Node 2 SIP
info "Creating Node 2 SIP quiddity..."
swctl --uri ${NODE2_URI} --create-quiddity sip sip
info "Setting Node 2 SIP access list mode to everybody..."
swctl --uri ${NODE2_URI} --set-prop sip mode 1
info "Setting Node 2 local SIP source port..."
swctl --uri ${NODE2_URI} --set-prop sip port ${SIP_SOURCE_PORT}
info "Setting Node 2 STUN/TURN servers..."
swctl --uri ${NODE2_URI} invoke sip set_stun_turn ${SIP_SERVER} ${SIP_SERVER} ${NODE2_SIP_USER} ${NODE2_SIP_PASSWORD}
info "Registering Node 1 SIP..."
swctl --uri ${NODE2_URI} invoke sip register ${NODE2_SIP_USER}@${SIP_SERVER}:${SIP_SERVER_PORT} ${NODE2_SIP_PASSWORD}
info "Adding Node 2 remote SIP contact..."
swctl --uri ${NODE2_URI} invoke sip add_buddy ${NODE1_SIP_USER}@${SIP_SERVER}

# Setting NORANDACAM1
NODE1_CAM1_NAME="${NODE1_SIP_USER^^}CAM1"
info "Setting ${NODE1_CAM1_NAME}..."
swctl --uri ${NODE1_URI} --create-quiddity sdiInput1 ${NODE1_CAM1_NAME}
swctl --uri ${NODE1_URI} --set-prop ${NODE1_CAM1_NAME} Capture/resolution ${VIDEO_RESOLUTION}
swctl --uri ${NODE1_URI} --set-prop ${NODE1_CAM1_NAME} Capture/standard_framerates ${VIDEO_FRAMERATE}
swctl --uri ${NODE1_URI} --set-prop ${NODE1_CAM1_NAME} Encoder/bitrate ${VIDEO_BITRATE}
swctl --uri ${NODE1_URI} --set-prop ${NODE1_CAM1_NAME} started true

# Setting NORANDACAM2
NODE1_CAM2_NAME="${NODE1_SIP_USER^^}CAM2"
info "Setting ${NODE1_CAM2_NAME}..."
swctl --uri ${NODE1_URI} --create-quiddity sdiInput2 ${NODE1_CAM2_NAME}
swctl --uri ${NODE1_URI} --set-prop ${NODE1_CAM2_NAME} Capture/resolution ${VIDEO_RESOLUTION}
swctl --uri ${NODE1_URI} --set-prop ${NODE1_CAM2_NAME} Capture/standard_framerates ${VIDEO_FRAMERATE}
swctl --uri ${NODE1_URI} --set-prop ${NODE1_CAM2_NAME} Encoder/bitrate ${VIDEO_BITRATE}
swctl --uri ${NODE1_URI} --set-prop ${NODE1_CAM2_NAME} started true

# Setting NORANDACAM3
NODE1_CAM3_NAME="${NODE1_SIP_USER^^}CAM3"
info "Setting ${NODE1_CAM3_NAME}..."
swctl --uri ${NODE1_URI} --create-quiddity hdmiInput ${NODE1_CAM3_NAME}
swctl --uri ${NODE1_URI} --set-prop ${NODE1_CAM3_NAME} Capture/resolution ${VIDEO_RESOLUTION}
swctl --uri ${NODE1_URI} --set-prop ${NODE1_CAM3_NAME} Capture/standard_framerates ${VIDEO_FRAMERATE}
swctl --uri ${NODE1_URI} --set-prop ${NODE1_CAM3_NAME} Encoder/bitrate ${VIDEO_BITRATE}
swctl --uri ${NODE1_URI} --set-prop ${NODE1_CAM3_NAME} started true

# Assign sources to remote
info "Assigning sources to remote..."
NODE1_CAM1_ENCODED_SHMPATH=$(swctl --uri ${NODE1_URI} --shmpath ${NODE1_CAM1_NAME} Encoder/video-encoded)
swctl --uri ${NODE1_URI} invoke sip attach_shmdata_to_contact ${NODE1_CAM1_ENCODED_SHMPATH} ${NODE2_SIP_USER}@$SIP_SERVER true
NODE1_CAM2_ENCODED_SHMPATH=$(swctl --uri ${NODE1_URI} --shmpath ${NODE1_CAM2_NAME} Encoder/video-encoded)
swctl --uri ${NODE1_URI} invoke sip attach_shmdata_to_contact ${NODE1_CAM2_ENCODED_SHMPATH} ${NODE2_SIP_USER}@$SIP_SERVER true
NODE1_CAM3_ENCODED_SHMPATH=$(swctl --uri ${NODE1_URI} --shmpath ${NODE1_CAM3_NAME} Encoder/video-encoded)
swctl --uri ${NODE1_URI} invoke sip attach_shmdata_to_contact ${NODE1_CAM3_ENCODED_SHMPATH} ${NODE2_SIP_USER}@$SIP_SERVER true

# Send Node 1 sources to remote
info "Sending Node 1 sources to remote..."
swctl --uri ${NODE1_URI} invoke sip send ${NODE2_SIP_USER}@$SIP_SERVER



# Setting GASPECAM1 (loopback)
NODE2_CAM1_NAME="${NODE2_SIP_USER^^}CAM1"
info "Setting ${NODE2_CAM1_NAME}..."
swctl --uri ${NODE2_URI} --create-quiddity videoEncoder ${NODE2_CAM1_NAME}
swctl --uri ${NODE2_URI} --set-prop ${NODE2_CAM1_NAME} Encoder/bitrate ${VIDEO_BITRATE}
swctl --uri ${NODE2_URI} --try-connect "NORANDACAM1 (Rouyn-Noranda | Petit Théatre du Vieux Noranda)" ${NODE2_CAM1_NAME}

# Setting GASPECAM2 (loopback)
NODE2_CAM2_NAME="${NODE2_SIP_USER^^}CAM2"
info "Setting ${NODE2_CAM2_NAME}..."
swctl --uri ${NODE2_URI} --create-quiddity videoEncoder ${NODE2_CAM2_NAME}
swctl --uri ${NODE2_URI} --set-prop ${NODE2_CAM2_NAME} Encoder/bitrate ${VIDEO_BITRATE}
swctl --uri ${NODE2_URI} --try-connect "NORANDACAM2 (Rouyn-Noranda | Petit Théatre du Vieux Noranda)" ${NODE2_CAM2_NAME}

# Setting GASPECAM3 (loopback)
NODE2_CAM3_NAME="${NODE2_SIP_USER^^}CAM3"
info "Setting ${NODE2_CAM3_NAME}..."
swctl --uri ${NODE2_URI} --create-quiddity videoEncoder ${NODE2_CAM3_NAME}
swctl --uri ${NODE2_URI} --set-prop ${NODE2_CAM3_NAME} Encoder/bitrate ${VIDEO_BITRATE}
swctl --uri ${NODE2_URI} --try-connect "NORANDACAM3 (Rouyn-Noranda | Petit Théatre du Vieux Noranda)" ${NODE2_CAM3_NAME}

# Assign sources to remote
info "Assigning Node 2 sources to remote..."
NODE2_CAM1_ENCODED_SHMPATH=$(swctl --uri ${NODE2_URI} --shmpath ${NODE2_CAM1_NAME} Encoder/video-encoded)
swctl --uri ${NODE2_URI} invoke sip attach_shmdata_to_contact ${NODE2_CAM1_ENCODED_SHMPATH} ${NODE1_SIP_USER}@$SIP_SERVER true
NODE2_CAM2_ENCODED_SHMPATH=$(swctl --uri ${NODE2_URI} --shmpath ${NODE2_CAM2_NAME} Encoder/video-encoded)
swctl --uri ${NODE2_URI} invoke sip attach_shmdata_to_contact ${NODE2_CAM2_ENCODED_SHMPATH} ${NODE1_SIP_USER}@$SIP_SERVER true
NODE2_CAM3_ENCODED_SHMPATH=$(swctl --uri ${NODE2_URI} --shmpath ${NODE2_CAM3_NAME} Encoder/video-encoded)
swctl --uri ${NODE2_URI} invoke sip attach_shmdata_to_contact ${NODE2_CAM3_ENCODED_SHMPATH} ${NODE1_SIP_USER}@$SIP_SERVER true

# Send Node 2 sources to remote
info "Sending Node 2 sources to remote..."
swctl --uri ${NODE2_URI} invoke sip send ${NODE1_SIP_USER}@$SIP_SERVER

# Done
info "Scenic connection completed!"
