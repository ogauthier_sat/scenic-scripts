#!/bin/bash

gst-launch-1.0 \
  jackaudiosrc \
      name=from_far_end \
      client-name=scenic_aec_far_end_src \
      connect=none \
  ! "audio/x-raw,format=(string)F32LE,layout=(string)interleaved,rate=(int)48000,channels=(int)8,channel-mask=(bitmask)0x0" \
  ! audioconvert \
  ! webrtcechoprobe \
  ! audioconvert \
  ! jackaudiosink \
      name=speaker \
      client-name=scenic_aec_speaker \
      connect=auto \
  jackaudiosrc \
      name=microphone \
      client-name=scenic_aec_microphone \
      connect=explicit \
      port-names="system:capture_1,system:capture_2,system:capture_3,system:capture_4,system:capture_5,system:capture_6,system:capture_7,system:capture_8" \
  ! "audio/x-raw,format=(string)F32LE,layout=(string)interleaved,rate=(int)48000,channels=(int)8,channel-mask=(bitmask)0x0" \
  ! audioconvert \
  ! webrtcdsp \
  ! audioconvert \
  ! jackaudiosink \
      name=to_far_end \
      client-name=scenic_aec_far_end_sink \
      connect=none
